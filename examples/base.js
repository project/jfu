$(function(){
  $(".modal-luncher, .modal-background, .modal-close").click(function () {
    $("body").toggleClass("modal-active");
    $(".modal-content,.modal-background").toggleClass("active");
  });

  $(".component-item--icon a").click(function () {
    $(".form-replace-inside").toggleClass("hidden");
    $(".form-component-inside").toggleClass("active");
    $("body").toggleClass("modal-active");
    $(".modal-content,.modal-background").toggleClass("active");
  });
});
  
