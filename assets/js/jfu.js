(function ($) {
  Drupal.behaviors.jfu = {
    attach: function(context, settings) {
      /* ACCORDION */
      $('.accordion-item--title-container').unbind('click');
      $('.accordion-item--title-container').click(function () {
        $(this).removeClass('is-active');

        if (!(false == $(this).next().is(':visible'))) {
          $(this).addClass('is-active');
        }   
        $(this).next().slideToggle(300);

        if ($('.jfu-toggle-trigger-icon').hasClass('is-active')) {
          $('.jfu-toggle-trigger-icon').removeClass('is-active');
        }
        else {
          $('.jfu-toggle-trigger-icon').addClass('is-active');
        }
      });

      /* GALLERY */
      $('.gallery-component').on("click", function(){
        $(this).find('.gallery-item--image a').colorbox({rel:'gal', maxWidth:'100%', maxHeight:'100%', onClosed:function(){ location.reload(true); }});
      });

      /* SCHEDULE */
      $('.schedule-item--program-item--extend .schedule-item--item-flag--extend').unbind('click');
      $('.schedule-item--program-item--extend .schedule-item--item-flag--extend').click(function () {
        $(this).parents('.schedule-item--program-item--extend').removeClass('is-active');

        if ((false == $(this).parents('.schedule-item--program-item--extend').find('.schedule-item--item-info .schedule-item--item--full-info').is(':visible'))) {
          $(this).parents('.schedule-item--program-item--extend').addClass('is-active');
        }   

        $(this).parents('.schedule-item--program-item--extend').find('.schedule-item--item-info .schedule-item--item--full-info').slideToggle(300);
      });

      /*SLIDESHOW*/
      $('.slideshow-slick').not('.slick-initialized').slick({
        arrows: true, 
        dots: true,
        speed: 300,
        slidesToShow: 1,
      });


      /* TABS */
      $('.tab-item--tab').click(function() {
        $(this).parent().find('.tab-item--tab').removeClass('is-active');
        $(this).addClass('is-active');
        var itemId = $(this).attr('data-tab-item');

        $(this).parents('.jfu-type--tabs').find('.tab-item--program--wrapper').removeClass('is-active');
        $(this).parents('.jfu-type--tabs').find('.tab-item--program--wrapper.'+ itemId).addClass('is-active');

        $(this).parents('.tab-item').find('.tab-item--program--wrapper.'+ itemId + ' .slideshow-slick').slick('setPosition');
        
      });


    }
  };
}(jQuery));
