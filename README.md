INSTALLATION
------------
Install and enable this module in the well-known way.

- HOW TO USE?
This module works in a JSON field type, it provides a format and a widget named "components" in both cases. It's necessary to choose this options.

When you choose the Component widget you can select which components will be able to use in this field, some of them has own configuration.

After that, you can see the field in the edit or create form and use the component.

COMPONENTS
----------
- QUOTE COMPONENT
This component is used to add a phrase with an author information (name, position, image).

- ACCORDION COMPONENT
This component has two main fields, one of them is the title which works as a switcher that controls if the content is displayed or not.

- BANNER COMPONENT
This component is used to show an image as a banner, you can add information like title, description and a link that you will see in front of the image.

- BLOCK COMPONENT
This component calls a Drupal module and using Twig tweak it's rendered. Also we added 2 fields for a title and a description, just to cover more possibilities.

The block component functionality depends on the `block_list_resource` endpoint, that its generated when the module jfu is installed otherwise if the module is installed we have to uninstall and install it again. Be careful with this change it could remove previous configurations, we recommend having a backup for this particular case.

The Block List Resource resource has a permission "Access GET on Block List Resource resource" we can control who can see the block list related to the block component.
   
- SIMPLE CARD
This component is used to show an image with a text next to it, the text can contain a Title, subtitle, description and a URL.

- CONTENT IN COLUMNS
This component adds 3 sections with an image and link for each one, also it has a main title with description for the component.


TO OVERRIDE TEMPLATES
--------------------
The way how templates render in this module is through includes for every kind of component so to override them it's necessary to copy the template json-widget-component.html.twig and place on your active theme. Clearing cache, of course.

We suggest declaring an array named `components` at the beginning of the template and every item that you set inside it will be the component which you want to override. 
Something like this:

        {%
          set components = [
            'quote',
            'name_component',
            'name_component2',
          ]
        %}

After that we need to add a conditional to change the place where the template will be, on the following example you can see the conditional which evaluate either our components are inside the array "components", to the other cases the templates continue calling from the module.

        {% if json_array.type %}
          {% if json_array.type in components %}
            {% include '@THEMENAME/jfu/' ~ json_array.type ~ '.html.twig' with { 'json_array': json_array } %}
          {% else %}
            {% include '@jfu/components/' ~ json_array.type ~ '.html.twig' with { 'json_array': json_array } %}
          {% endif %}
        {% endif %}

Finally, to get the functionality. According to our example the template should be in the estructure defined `"THEMENAME/templates/jfu/"`, you can copy from the module `jfu/templates/components/nameofcomponent.html.twig` and place on your active theme and modify whatever you want.

Don't forget to clear caché for new templates.

CKEDITOR IN JSON FIELD UTILS
----------------------------
This module use CKEDITOR in some textarea fields, the way we added it, was a little bit different. We added the configuration by PHP and send values to the VUEJS code, in that file we call the CKEDITOR instance and execute the .replace() method for the respective field.

NOTE
----
If you are using Json Field Utils module 1.1.0-alpha15 or higher you should use Json Field module 8.x-1.1.
If you are using a lower version of Json Field Utils you should use Json Field module 8.x-1.0-rc4.
For version 9.5 and above, install the CKEditor module (Deprecated), clear cache.