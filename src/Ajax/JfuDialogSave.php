<?php

namespace Drupal\jfu\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Provides an AJAX command for saving the image json field utils dialog.
 *
 * This command is implemented in jfu.dialog.js in
 * Drupal.AjaxCommands.prototype.JfusDialogSave.
 */
class JfuDialogSave implements CommandInterface {

  /**
   * An array of values that will be passed back to json field utils by the dialog.
   *
   * @var string
   */
  protected $values;

  /**
   * Constructs a JfuDialogSave object.
   *
   * @param string $values
   *   The values that should be passed to the form constructor in Drupal.
   */
  public function __construct($values) {
    $this->values = $values;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'jfuDialogSave',
      'values' => $this->values,
    ];
  }

}
