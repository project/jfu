<?php

namespace Drupal\jfu\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\json_field\Plugin\Field\FieldWidget\JsonTextareaWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Environment;

/**
 * Plugin implementation of the 'json_component_widget' widget.
 *
 * @FieldWidget(
 *   id = "json_component_widget",
 *   module = "jfu",
 *   label = @Translation("Component"),
 *   description = @Translation("Define all components."),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary"
 *   },
 * )
 */

class JSONComponentWidget extends JsonTextareaWidget {
  
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $components = [
      'quote' => 'quote',
      'accordion' => 'accordion',
      'banner' => 'banner',
      'block' => 'block',
      'tabs' => 'tabs',
      'embed' => 'embed',
      'gallery' => 'gallery',
      'simple_card' => 'simple_card',
      'content_columns' => 'content_columns',
      'data_number_card' => 'data_number_card',
      'rich_text' => 'rich_text',
      'table' => 'table',
      'link_list' => 'link_list'
    ];

    $components_config = [
      'quote' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'accordion' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'banner' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'max_dimensions_responsive' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'block' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'tabs' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'embed' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'gallery' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'simple_card' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'content_columns' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
        'allow_group_items' => 0
      ],
      'data_number_card' => [
        'directory' => 'jfu-images',
        'max_size' => '',
        'max_dimensions' => [
          'width' => '',
          'height' => ''
        ],
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
        'allow_group_items' => 0
      ],
      'rich_text' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'table' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
      ],
      'link_list' => [
        'allow_classes' => 0,
        'allow_select_class' => 0,
        'class_list' => '',
        'allow_group_items' => 0
      ],
    ];

    $added_components = [];
    \Drupal::moduleHandler()->invokeAllWith('jfu_add_components',
      function (callable $hook, string $module) use (&$added_components) {
        $hook($added_components);
      }
    );

    \Drupal::moduleHandler()->alter('jfu_add_components', $added_components);

    //TODO: Check components names in order to prevent name collision.
    if(!empty($added_components)) {
      foreach ($added_components as $key => $value) {
        $components[$key] = $key;
        $components_config[$key] = $value;

        if(isset($value['image']) && $value['image']) {
          $components_config[$key]['directory'] = 'jfu-images';
          $components_config[$key]['max_size'] = '';
          $components_config[$key]['max_dimensions'] = [
            'width' => '',
            'height' => ''
          ];
        }

        if(isset($value['responsive_image']) && $value['responsive_image']) {
          $components_config[$key]['max_dimensions_responsive'] = [
            'width' => '',
            'height' => ''
          ];
        }

        //TODO: Probably will not need to be removed when restructuring the components.
        unset($components_config[$key]['image']);
        unset($components_config[$key]['responsive_image']);
      }
    }

    return [
      'components' => $components,
      'components_config' => $components_config,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if (!isset($form['#is_jfu'])) {
      $form['#is_jfu'] = 'field--widget-json-component-widget';
    }

    $components = $this->getSetting('components');
    $components_config = $this->getSetting('components_config');
    $jfu_components = [];

    foreach ($components as $name) {
      if ($name !== 0) {
        $module_name = 'jfu';
        if(isset($components_config[$name]['module_name']) && !empty($components_config[$name]['module_name'])) {
          $module_name = $components_config[$name]['module_name'];
        }

        $extension_path_resolver = \Drupal::service('extension.path.resolver');
        $cmd = DRUPAL_ROOT . '/' . $extension_path_resolver->getPath('module', $module_name) . '/component_models/';
        $ufc = $cmd . $name . '/' . $name . '.json';
        $json_string = file_get_contents($ufc);
        $jfu_components[$name] = Json::decode($json_string);

        //TODO: Key module name is added in the json to select the corresponding template avaluate other options.
        $jfu_components[$name]['module_name'] = $module_name;
      }
    }

    $field_storage_definition = $items->getFieldDefinition()->getFieldStorageDefinition();
    $field_name = $field_storage_definition->getName();
    $form['#attached']['drupalSettings']['jfu_components'][$field_name] = $jfu_components;
    
    $element['value'] = $element + [
      '#type' => 'textarea',
      '#default_value' => $items[$delta]->value,
      '#attributes' => [
        'target-weight' => $delta,
      ],
    ];
    
    return $element;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $components = $this->getSetting('components');
    $components_config = $this->getSetting('components_config');
    $options = [];
    foreach ($components as $key => $name) {
      $options[$key] = ucfirst(str_replace('_', ' ', $key));
    }

    $element['components'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Components list'),
      '#options' => $options,
      '#default_value' => $components,
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];

    $element['components_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Components configuration'),
    ];

    // Components whitout image
    $components_whitout_image = [];

    //Components whit responsive image.
    foreach ($components_config as $key_config => $config) {
      //TODO: This condition was placed provisionally but a refactor is necesary, because a component can have several images and/or other types of files.
      if(!isset($config['directory'])) {
        $components_whitout_image[$key_config] = $key_config;
      }

      $element['components_config'][$key_config] = [
        '#type' => 'details',
        '#title' => $this->t('Config ' . $key_config),
        '#states' => [
          'visible' => [
            ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
          ],
        ],
      ];

      if (!in_array($key_config, $components_whitout_image)) {
        $element['components_config'][$key_config]['directory'] = [
          '#type' => 'textfield',
          '#default_value' => $config['directory'],
          '#title' => t('Upload directory'),
          '#description' => t("A directory relative to Drupal's files directory where uploaded images will be stored."),
          '#states' => [
            'visible' => [
              ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $default_max_size = format_size(Environment::getUploadMaxSize());
        $element['components_config'][$key_config]['max_size'] = [
          '#type' => 'textfield',
          '#default_value' => $config['max_size'],
          '#title' => t('Maximum file size'),
          '#description' => t('If this is left empty, then the file size will be limited by the PHP maximum upload size of @size.', ['@size' => $default_max_size]),
          '#maxlength' => 20,
          '#size' => 10,
          '#placeholder' => $default_max_size,
          '#states' => [
            'visible' => [
              ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $element['components_config'][$key_config]['max_dimensions'] = [
          '#type' => 'item',
          '#title' => t('Maximum dimensions'),
          '#description' => t('Images larger than these dimensions will be scaled down.'),
          '#states' => [
            'visible' => [
              ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $element['components_config'][$key_config]['max_dimensions']['width'] = [
          '#title' => t('Width'),
          '#title_display' => 'invisible',
          '#type' => 'number',
          '#default_value' => (empty($config['max_dimensions']['width'])) ? '' : $config['max_dimensions']['width'],
          '#size' => 8,
          '#maxlength' => 8,
          '#min' => 1,
          '#max' => 99999,
          '#placeholder' => t('width'),
          '#field_suffix' => ' x ',
          '#states' => [
            'visible' => [
              ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
            ],
          ],
          '#prefix' => '<div class="form--inline clearfix">',
        ];

        $element['components_config'][$key_config]['max_dimensions']['height'] = [
          '#title' => t('Height'),
          '#title_display' => 'invisible',
          '#type' => 'number',
          '#default_value' => (empty($config['max_dimensions']['height'])) ? '' : $config['max_dimensions']['height'],
          '#size' => 8,
          '#maxlength' => 8,
          '#min' => 1,
          '#max' => 99999,
          '#placeholder' => t('height'),
          '#field_suffix' => t('pixels'),
          '#states' => [
            'visible' => [
              ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
            ],
          ],
          '#suffix' => '</div>',
        ];

        if (isset($config['max_dimensions_responsive']) && !empty($config['max_dimensions_responsive'])) {
          $element['components_config'][$key_config]['max_dimensions_responsive'] = [
            '#type' => 'item',
            '#title' => t('Maximum dimensions responsive'),
            '#description' => t('Images larger than these dimensions will be scaled down.'),
            '#states' => [
              'visible' => [
                ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
              ],
            ],
          ];

          $element['components_config'][$key_config]['max_dimensions_responsive']['width'] = [
            '#title' => t('Width'),
            '#title_display' => 'invisible',
            '#type' => 'number',
            '#default_value' => (empty($config['max_dimensions_responsive']['width'])) ? '' : $config['max_dimensions_responsive']['width'],
            '#size' => 8,
            '#maxlength' => 8,
            '#min' => 1,
            '#max' => 99999,
            '#placeholder' => t('width'),
            '#field_suffix' => ' x ',
            '#states' => [
              'visible' => [
                ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
              ],
            ],
            '#prefix' => '<div class="form--inline clearfix">',
          ];

          $element['components_config'][$key_config]['max_dimensions_responsive']['height'] = [
            '#title' => t('Height'),
            '#title_display' => 'invisible',
            '#type' => 'number',
            '#default_value' => (empty($config['max_dimensions_responsive']['height'])) ? '' : $config['max_dimensions_responsive']['height'],
            '#size' => 8,
            '#maxlength' => 8,
            '#min' => 1,
            '#max' => 99999,
            '#placeholder' => t('height'),
            '#field_suffix' => t('pixels'),
            '#states' => [
              'visible' => [
                ':input[value="' . $key_config . '"]' => ['checked' => TRUE],
              ],
            ],
            '#suffix' => '</div>',
          ];
        }
      }

      $element['components_config'][$key_config]['allow_classes'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Allows you to add classes to the components container'),
        '#default_value' => empty($config['allow_classes']) ? '' : $config['allow_classes'],
      ];

      $element['components_config'][$key_config]['allow_select_class'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Allows defining custom classes for the component container'),
        '#default_value' => empty($config['allow_select_class']) ? '' : $config['allow_select_class'],
      ];

      $element['components_config'][$key_config]['class_list'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Add custom classes'),
        '#description' => $this->t('Add classes separated by "," and without spaces. For example: class1,class-2,class_3.'),
        '#default_value' => empty($config['class_list']) ? '' : $config['class_list'],
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][components_config][' . $key_config . '][allow_select_class]"]' => ['checked' => TRUE],
          ],
        ],
      ];

      if(isset($config['allow_group_items'])) {
        $element['components_config'][$key_config]['allow_group_items'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Allows you to add group items to the components'),
          '#default_value' => empty($config['allow_group_items']) ? '' : $config['allow_group_items'],
        ];
      }

      //TODO: Need a review
      if(isset($config['module_name']) && !empty($config['module_name'])) {
        $element['components_config'][$key_config]['module_name'] = [
          '#type' => 'hidden',
          '#value' => $config['module_name'],
        ];
      }
    }

    return $element;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    
    $items = [];
    foreach ($this->getSetting('components') as $component) {
      if ($component !== 0) {
        $items[] = ucfirst(str_replace('_', ' ', $component));
      }
    }

    $list_components = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Allowed components'),
      '#items' => $items,
    ];
    
    $summary[] = $list_components;

    return $summary;
  }

}
