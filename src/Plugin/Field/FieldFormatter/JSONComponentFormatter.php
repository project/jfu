<?php

namespace Drupal\jfu\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Serialization\Json;

/**
 * Plugin implementation of the 'json_component_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "json_component_formatter",
 *   label = @Translation("Components"),
 *   field_types = {
 *     "json",
 *     "json_native",
 *     "json_native_binary",
 *   },
 * )
 */

class JSONComponentFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
  */
  public static function defaultSettings() {
    $components_config = [
      'data_number_card' => [
        'data_delay' => 5,
        'data_increment' => 10,
      ],
    ];

    return [
      'format_settings_components' => $components_config,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
  */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $components_config = $this->getSetting('format_settings_components');

    $form['format_settings_components'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Format Settings Components'),
    ];

    $form['format_settings_components']['data_number_card'] = [
      '#type' => 'details',
      '#title' => $this->t('Config number card'),
    ];

    $form['format_settings_components']['data_number_card']['data_delay'] = [
      '#type' => 'number',
      '#default_value' => $components_config['data_number_card']['data_delay'],
      '#title' => t('Data delay'),
      '#description' => t("A number data delay for numscroller."),
      '#min' => 5,
      '#required' => TRUE,
    ];

    $form['format_settings_components']['data_number_card']['data_increment'] = [
      '#type' => 'number',
      '#default_value' => $components_config['data_number_card']['data_increment'],
      '#title' => t('Data increment'),
      '#description' => t("A number data increment for numscroller."),
      '#min' => 10,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $components_config = $this->getSetting('format_settings_components');
    $summary = [];
    $summary[] = $this->t('Displays all components.');
    $summary[] = 'Config numscroller = Data delay : ' . $components_config['data_number_card']['data_delay'] . ' and Data increment : ' . $components_config['data_number_card']['data_increment'];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $template = '';
    \Drupal::moduleHandler()->invokeAllWith('jfu_add_template',
      function (callable $hook, string $module) use (&$template) {
        $hook($template);
      }
    );
    \Drupal::moduleHandler()->alter('jfu_add_template', $template);

    $components_config = $this->getSetting('format_settings_components');
    $elements = [];
    $json_array = [];

    $json_type_array = [];

    foreach ($items as $key => $item) {
      $json_array[$key] = Json::decode($item->value);

      $json_type_array = $json_array[$key];

      $classes = '';
      if (!empty($json_type_array['options']['custom_classes'])) {
        $clean_string_classes = str_replace(' ', '', $json_type_array['options']['custom_classes']);
        $classes .= ' ' . str_replace(',', ' ', $clean_string_classes);
      }

      if (!empty($json_type_array['options']['selected_class'])) {
        $classes .= ' ' . $json_type_array['options']['selected_class'];
      }

      $jfu_template = 'json_widget_component';
      if(isset($json_type_array['module_name']) && $json_type_array['module_name'] != 'jfu' && !empty($template)) {
        $jfu_template = $template;
      }


      if(isset($json_type_array['options']['group_items_quantity'])
        && isset($json_type_array['options']['group_classes'])
        && !empty($json_type_array['options']['group_items_quantity'])
        && $json_type_array['options']['group_items_quantity'] != 0) {
        $json_type_array = $this->jfuAddGroupItems($json_type_array);

      }

      $elements[] = [
        '#theme' => $jfu_template,
        '#values' => $json_array,
        '#json_array' => $json_type_array,
        '#components_config' => $components_config,
        '#classes' => $classes,
      ];
    }

    return $elements;
  }

  private function jfuAddGroupItems($json_type_array) {
    $group_items = [];
    $key_init = 0;
    $number_init = 0;
    foreach ($json_type_array['items']['items'] as $key => $value) {
      $number_init ++;
      if($number_init > $json_type_array['options']['group_items_quantity']){
        $number_init = 1;
        $key_init = $key_init + 1;
      }

      $group_items[$key_init][$key] = $value;
    }

    $group_classes = '';
    if (!empty($json_type_array['options']['group_classes'])) {
      $clean_string_classes = str_replace(' ', '', $json_type_array['options']['group_classes']);
      $group_classes .= ' ' . str_replace(',', ' ', $clean_string_classes);
    }

    $json_type_array['items']['group_items'] = $group_items;
    $json_type_array['items']['group_classes'] = $group_classes;

    return $json_type_array;
  }
}
