<?php

namespace Drupal\jfu\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
  * Provides a Block list resource
  *
  * @RestResource (
  *   id = "block_list_resource",
  *   label = @Translation("Block List Resource"),
  *   uri_paths = {
  *     "canonical" = "/jfu_rest_api/block_list_resource"
  *   }
  * )
 **/

class BlockListResource extends ResourceBase {
  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */

  public function get() {
    $blockManager = \Drupal::service('plugin.manager.block');
    $contextRepository = \Drupal::service('context.repository');
    $definitions = $blockManager->getDefinitionsForContexts(
        $contextRepository->getAvailableContexts()
    );

    $definitionsArray = json_decode(json_encode($definitions), true);
    return new ResourceResponse($definitionsArray);
  }
}
